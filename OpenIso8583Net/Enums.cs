﻿using System;
namespace OpenIso8583Net
{
    /// <summary>
    /// Enum describing field types
    /// </summary>
    [Serializable]
    public enum FieldType
    {
        /// <summary>
        /// Fixed length field
        /// </summary>
        Fixed,
        /// <summary>
        /// Variable length field
        /// </summary>
        Variable
    }
}
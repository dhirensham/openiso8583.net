﻿using System;
using System.Collections.Generic;

namespace OpenIso8583Net
{
    /// <summary>
    ///   Class representing a Ministatement Line
    /// </summary>
    [Serializable]
    public class MinistatementLine : Dictionary<string, string>
    {
        public MinistatementLine()
            : base()
        {
        }

        public MinistatementLine(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context) 
        {
        }
    }
}